import { CSSTransition } from 'react-transition-group';
import React from 'react';

const FadeAnimation = ({ children, ...props }) => (
  <CSSTransition
    {...props}
    mountOnEnter
    unmountOnExit
    timeout={500}
    classNames="fade"
  >
    {children}
  </CSSTransition>
);

export default FadeAnimation;
