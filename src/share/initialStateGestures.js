let initialStateGestures = [
  {
    name: 'lizard',
    img: '../images/lizard.svg',
    win: ['spock', 'paper']
  },
  {
    name: 'paper',
    img: '../images/paper.svg',
    win: ['rock', 'spock']
  },
  {
    name: 'rock',
    img: '../images/rock.svg',
    win: ['lizard', 'scissors']
  },
  {
    name: 'scissors',
    img: '../images/scissors.svg',
    win: ['paper', 'lizard']
  },
  {
    name: 'spock',
    img: '../images/spock.svg',
    win: ['rock', 'scissors']
  }
];

export default initialStateGestures;
