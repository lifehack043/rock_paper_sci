import React from 'react';
import Game from './components/Game';
import Chat from './components/chat';
import io from 'socket.io-client';
import FadeAnimation from './share/helpers';
import '../public/stylesheets/App.scss';
import '../public/stylesheets/animation.scss';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activateNameForm: false,
      nameSubmitted: false,
      activateWaitMessage: false,
      activateCreateOrJoinForm: false,
      roomCreated: false,
      gameCode: 0,
      playerName: '',
      playerNum: '',
      anotherPlayerNum: '',
      createCode: '',
      joinCode: '',
      start: false,
      errMessageWhenJoinRoom: ''
    };
  }
  componentWillMount() {
    this.socket = io('/');
  }

  componentDidMount() {
    this.setState({ activateNameForm: true });

    this.socket.on('game start', data => {
      console.log('game start');
      this.setState({
        gameCode: this.state.createCode,
        playerNames: data,
        activateWaitMessage: false,
        activateCreateOrJoinForm: false
      });
    });

    this.socket.on('errMessageWhenJoinRoom', errMessage => {
      this.setState({ errMessageWhenJoinRoom: errMessage });
    });
  }

  handleSubmitName = event => {
    event.preventDefault();
    this.setState({ activateNameForm: false });
  };

  handleActivateCreateOrJoinForm = () => {
    this.setState({ activateCreateOrJoinForm: true });
  };
  handleChangeName = event => {
    event.preventDefault();
    this.setState({ playerName: event.target.value });
  };

  handleChangeJoin = event => {
    this.setState({ joinCode: event.target.value });
  };

  handleActivateWaitMessage = () => {
    this.setState({ activateWaitMessage: true });
  };

  handleGameStart = () => {
    this.setState({ start: true });
  };

  createRoom = event => {
    event.preventDefault();
    let roomCode = parseInt(Math.random() * (99999 - 11111), 10);
    this.setState({
      playerNum: 1,
      anotherPlayerNum: 2,
      createCode: roomCode,
      activateCreateOrJoinForm: false,
      roomCreated: true
    });
    this.socket.emit('create room', {
      player1Name: this.state.playerName,
      roomCode: roomCode
    });
  };

  joinRoom = event => {
    event.preventDefault();
    this.socket.emit('join room', {
      player2Name: this.state.playerName,
      roomCode: this.state.joinCode
    });
    this.setState({
      playerNum: 2,
      anotherPlayerNum: 1,
      createCode: this.state.joinCode
    });
  };

  render() {
    return (
      <div>
        <FadeAnimation
          in={this.state.activateNameForm}
          onExited={this.handleActivateCreateOrJoinForm}
        >
          <form className="lobby__form" onSubmit={this.handleSubmitName}>
            <label className="lobby__label">Name:</label>
            <input
              type="text"
              className="lobby__input"
              placeholder={'Your name'}
              onChange={this.handleChangeName}
              value={this.state.playerName}
              required
            />
            <button className="lobby__button">Submit</button>
          </form>
        </FadeAnimation>

        <FadeAnimation
          in={this.state.activateCreateOrJoinForm}
          onExited={
            this.state.roomCreated
              ? this.handleActivateWaitMessage
              : this.handleGameStart
          }
        >
          <form className="lobby__form">
            <button className="lobby__button" onClick={this.createRoom}>
              Create Game Room
            </button>
            <label className="lobby__label">
              {this.state.errMessageWhenJoinRoom}
            </label>
            <input
              className="lobby__input"
              type="text"
              placeholder="Insert code here..."
              value={this.state.joinCode}
              onChange={this.handleChangeJoin}
            />
            <button
              className="lobby__button"
              type="submit"
              onClick={this.joinRoom}
            >
              Join Game
            </button>
          </form>
        </FadeAnimation>

        <FadeAnimation
          in={this.state.activateWaitMessage}
          onExited={this.handleGameStart}
        >
          <form className="lobby__form">
            <label className="lobby__label">
              <span>Send this code: </span>
              <h2>{this.state.createCode}</h2>
              <p>Waiting for opponent...</p>
            </label>
          </form>
        </FadeAnimation>

        <FadeAnimation in={this.state.start}>
          <div className="GameAndChat">
            <Game
              playerNum={this.state.playerNum}
              anotherPlayerNum={this.state.anotherPlayerNum}
              playerName={this.state.playerName}
              socket={this.socket}
              gameCode={this.state.gameCode}
              playerNames={this.state.playerNames}
            />

            <Chat
              socket={this.socket}
              playerNum={this.state.playerNum}
              anotherPlayerNum={this.state.anotherPlayerNum}
              gameCode={this.state.gameCode}
            />
          </div>
        </FadeAnimation>
      </div>
    );
  }
}

export default App;
