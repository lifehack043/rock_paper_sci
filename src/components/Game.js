import React from 'react';
import initialStateGestures from '../share/initialStateGestures';
import FadeAnimation from '../share/helpers';
import '../../public/stylesheets/Game.scss';

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      playerName: props.playerName,
      playerNum: props.playerNum,
      anotherPlayerNum: props.anotherPlayerNum,
      playerNames: props.playerNames,
      socket: props.socket,
      gameCode: props.gameCode,
      winCount: {
        player1: 0,
        player2: 0
      },
      playersChoice: {
        player1: null,
        player2: null
      },
      readyButton: false,
      showGestures: false,
      showChoicedGestures: false,
      sendChoice: false,
      anotherPlayerChoiced: false,
      waitAnotherPlayer: false,
      winner: '',
      gestures: initialStateGestures
    };
  }

  componentDidMount() {
    this.setState({
      showGestures: true
    });

    this.state.socket.on('anotherPlayerChoiced', playerNum => {
      this.setState({ anotherPlayerChoiced: true });
    });

    this.state.socket.on('game end', result => {
      this.setState({
        playersChoice: { ...this.state.playersChoice, ...result.playersChoice },
        waitAnotherPlayer: false,
        sendChoice: false,
        winner: result.winner,
        winCount: {
          player1: result.winCount.player1,
          player2: result.winCount.player2
        }
      });
    });

    this.state.socket.on('continue game', data => {
      this.setState(prevState => ({
        showChoicedGestures: false,
        anotherPlayerChoiced: false,
        readyButton: false,
        gestures: initialStateGestures
      }));
    });
  }

  onPlayerChoice = choiceGesture => {
    this.setState({
      sendChoice: true,
      showGestures: false
    });
    this.state.socket.emit('choice gesture', {
      gameCode: this.state.gameCode,
      playerNum: this.state.playerNum,
      anotherPlayerNum: this.state.anotherPlayerNum,
      playerChoice: choiceGesture
    });
  };

  onContinueGame = () => {
    this.setState({ readyButton: !this.state.readyButton });
    this.state.socket.emit('continue game', {
      gameCode: this.state.gameCode,
      playerNum: this.state.playerNum
    });
  };

  onWaitAnotherPlayer = () => {
    this.setState({ waitAnotherPlayer: true });
  };

  onShowChoicedGestures = () => {
    this.setState({ showChoicedGestures: true });
  };

  onShowGestures = () => {
    this.setState({ showGestures: true });
  };

  render() {
    return (
      <section className="game__game_container">
        <div className="game_container__statistic">
          <label>
            {this.state.playerNames.player1} {this.state.winCount.player1} :
            {this.state.winCount.player2} {this.state.playerNames.player2}
          </label>
        </div>

        <div className="game_container__board">
          <FadeAnimation
            in={this.state.waitAnotherPlayer}
            onExited={this.onShowChoicedGestures}
            classNames="fade"
          >
            <span className="board__information">
              Wait{' '}
              {this.state.playerNames[`player${this.state.anotherPlayerNum}`]}{' '}
              turn...
            </span>
          </FadeAnimation>

          <FadeAnimation
            in={this.state.showChoicedGestures}
            onExited={this.onShowGestures}
          >
            <div className="board__choice_container">
              <span className="board__information">
                {' '}
                {`${this.state.winner}`} win this game!{' '}
              </span>

              <div className="board__choiced_gestures">
                <div className="choiced_gestures__choiced_gesture">
                  <img
                    className="choiced_gesture__image"
                    src={
                      this.state.playersChoice[`player${this.state.playerNum}`]
                    }
                  />
                  <label className="choiced_gesture__label">Your choice</label>
                </div>

                <div className="choiced_gestures__choiced_gesture">
                  <img
                    className="choiced_gesture__image"
                    src={
                      this.state.playersChoice[
                        `player${this.state.anotherPlayerNum}`
                      ]
                    }
                  />
                  <label className="choiced_gesture__label">
                    Opponent choice
                  </label>
                </div>
              </div>

              <div className="board__wrapper_ready_button">
                <button
                  onClick={this.onContinueGame.bind(this)}
                  className={
                    this.state.readyButton
                      ? 'board__unreadyButton'
                      : 'board__readyButton'
                  }
                >
                  Ready
                </button>
              </div>
            </div>
          </FadeAnimation>

          <FadeAnimation
            in={this.state.showGestures}
            onExited={
              this.state.anotherPlayerChoiced
                ? this.onShowChoicedGestures
                : this.onWaitAnotherPlayer
            }
          >
            <div className="board__gestures_list">
              {this.state.gestures.map(gesture => (
                <img
                  className="gestures_list__gesture_image"
                  key={gesture.name}
                  src={gesture.img}
                  alt={gesture.name}
                  onClick={this.onPlayerChoice.bind(this, gesture)}
                />
              ))}
            </div>
          </FadeAnimation>
        </div>
      </section>
    );
  }
}

export default Game;
