import React from 'react';
import '../../public/stylesheets/chat.scss';
import io from 'socket.io-client';
import Sound from 'react-sound';

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      socket: this.props.socket,
      gameCode: this.props.gameCode,
      playerNum: this.props.playerNum,
      anotherPlayerNum: this.props.anotherPlayerNum,
      msgReceivedSound: false
    };
  }

  componentDidMount() {
    this.state.socket.on('message', message => {
      this.setState({
        messages: [message, ...this.state.messages],
        msgReceivedSound: message.from !== this.state.playerNum ? true : false
      });
    });
  }

  submitMessage = event => {
    if (event.keyCode === 13 && event.target.value !== '') {
      this.state.socket.emit('message', {
        gameCode: this.state.gameCode,
        time: new Date(),
        body: event.target.value,
        from: this.state.playerNum
      });
      event.target.value = '';
    }
  };

  render() {
    return (
      <div className="chat__container">
        <div className="chat__messages">
          <ul className="chat__messages_fix">
            {this.state.messages.map((message, index) => (
              <li
                key={index}
                className={
                  this.state.playerNum === message.from
                    ? 'chat__message__your'
                    : 'chat__message__opponent'
                }
              >
                {message.body}
              </li>
            ))}
          </ul>
        </div>
        <Sound
          url="msn_alert.mp3"
          volume={30}
          playbackRate={1}
          playStatus={
            Sound.status[this.state.msgReceivedSound ? `PLAYING` : null]
          }
        />
        <input className="chat__input" onKeyUp={this.submitMessage} />
      </div>
    );
  }
}

export default Chat;
