const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const socketIo = require('socket.io');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackConfig = require('./webpack.config.js');

const app = express();
const server = http.createServer(app);
const io = socketIo(server);
app.use(bodyParser.urlencoded({ extended: false }));

let gameBoardStore = {
  checkWinner: function() {
    if (this.player1.choice.win.includes(this.player2.choice.name)) {
      this.winCount.player1++;
      return this.player1.name;
    }
    if (this.player2.choice.win.includes(this.player1.choice.name)) {
      this.winCount.player2++;
      return this.player2.name;
    } else {
      return 'friendship';
    }
  }
};

io.on('connection', socket => {
  socket.on('create room', data => {
    if (data.roomCode) {
      delete gameBoardStore[data.roomCode];
      gameBoardStore[data.roomCode] = {
        winCount: {
          player1: 0,
          player2: 0
        },
        full: false
      };
      gameBoardStore[data.roomCode].player1 = {
        name: data.player1Name,
        socketId: socket.id,
        choice: null,
        approveContinue: false
      };
      socket.join(data.roomCode);
    }
  });

  // connecting player to room (using roomCode given bu host player)

  socket.on('join room', data => {
    if (gameBoardStore.hasOwnProperty(data.roomCode)) {
      if (gameBoardStore[data.roomCode].full != true) {
        gameBoardStore[data.roomCode].full = true;
        gameBoardStore[data.roomCode]['player2'] = {
          name: data.player2Name,
          socketId: socket.id,
          choice: null,
          approveContinue: false
        };
        socket.join(data.roomCode);
        io.to(data.roomCode).emit('game start', {
          player1: gameBoardStore[data.roomCode].player1.name,
          player2: gameBoardStore[data.roomCode].player2.name
        });
      } else {
        socket.emit('errMessageWhenJoinRoom', 'these room is full');
      }
    } else {
      socket.emit('errMessageWhenJoinRoom', 'there is no such rooms');
    }
  });

  socket.on('choice gesture', data => {
    if (data.gameCode in gameBoardStore) {
      gameBoardStore[data.gameCode][`player${data.playerNum}`].choice =
        data.playerChoice;
      io.to(
        gameBoardStore[data.gameCode][`player${data.anotherPlayerNum}`].socketId
      ).emit('anotherPlayerChoiced', data.playerNum);
      let { player1, player2 } = gameBoardStore[data.gameCode];
      if (player1.choice != null && player2.choice != null) {
        let winner = gameBoardStore.checkWinner.call(
          gameBoardStore[data.gameCode]
        );
        io.to(data.gameCode).emit('game end', {
          winner: winner,
          winCount: gameBoardStore[data.gameCode].winCount,
          playersChoice: {
            player1: player1.choice.img,
            player2: player2.choice.img
          }
        });
        gameBoardStore[data.gameCode].player1.choice = null;
        gameBoardStore[data.gameCode].player2.choice = null;
      }
    }
  });

  socket.on('continue game', data => {
    if (data.gameCode in gameBoardStore) {
      gameBoardStore[data.gameCode][
        `player${data.playerNum}`
      ].approveContinue = !gameBoardStore[data.gameCode][
        `player${data.playerNum}`
      ].approveContinue;
      let { player1, player2 } = gameBoardStore[data.gameCode];

      if (
        player1.approveContinue === true &&
        player2.approveContinue === true
      ) {
        gameBoardStore[data.gameCode].player1.approveContinue = false;
        gameBoardStore[data.gameCode].player2.approveContinue = false;
        io.to(data.gameCode).emit('continue game', 'game continue');
      }
    }
  });

  socket.on('message', message => {
    io.to(message.gameCode).emit('message', {
      body: message.body,
      time: message.time,
      from: message.from
    });
  });
});

app.use(express.static(__dirname + '/public'));
app.use(
  webpackDevMiddleware(webpack(webpackConfig), {
    noInfo: true,
    publicPath: webpackConfig.output.publicPath
  })
);
app.use(require('webpack-hot-middleware')(webpack(webpackConfig)));

server.listen(process.env.PORT || 8080);
